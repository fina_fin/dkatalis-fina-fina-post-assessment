package com.post.assestment.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BusinessException extends Exception {
	private static final long serialVersionUID = 1L;
	private final String errorMessage;

	public BusinessException(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}
