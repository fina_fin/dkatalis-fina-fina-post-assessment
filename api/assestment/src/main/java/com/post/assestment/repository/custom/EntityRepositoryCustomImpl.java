package com.post.assestment.repository.custom;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;

import com.post.assestment.domain.Entity;

public class EntityRepositoryCustomImpl {
	private final MongoTemplate mongoTemplate;

	@Autowired
	public EntityRepositoryCustomImpl(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}
	
	public List<Entity> getCountEntity() {
		Aggregation aggregation = Aggregation.newAggregation(
				group("status"));

		AggregationResults<Entity> result = mongoTemplate.aggregate(aggregation, Entity.class,
				Entity.class);
		return result.getMappedResults();
	}


}
