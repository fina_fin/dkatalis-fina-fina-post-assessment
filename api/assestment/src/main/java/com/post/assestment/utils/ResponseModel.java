package com.post.assestment.utils;

import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseModel<T> {

	@JsonProperty("responseCode")
	private String responseCode;

	@JsonProperty("message")
	private String responseDesc;

	@JsonProperty("data")
	private T dataResponse;

	public ResponseModel() {
	}

	public ResponseModel(String responseCode, String responseDesc, T dataResponse) {
		super();
		this.responseCode = responseCode;
		if (!StringUtils.isEmpty(responseDesc))
			this.responseDesc = responseCode.concat(" - ").concat(responseDesc);
		this.dataResponse = dataResponse;
	}

}
