package com.post.assestment.config;

import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackages = { "com.post.assestment.repository" })
public class MongoDbConfig {

}
