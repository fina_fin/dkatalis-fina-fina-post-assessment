package com.post.assestment.exception;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.post.assestment.constant.ApplicationConstant;
import com.post.assestment.utils.ResponseModel;

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
public class ExceptionControllerAdvice {

	@ExceptionHandler(BusinessException.class)
	public ResponseEntity<ResponseModel<Object>> handleApplicationException(BusinessException e) {
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
				.body(new ResponseModel<>(ApplicationConstant.MSG_ERROR, e.getErrorMessage(), null));
	}

}
