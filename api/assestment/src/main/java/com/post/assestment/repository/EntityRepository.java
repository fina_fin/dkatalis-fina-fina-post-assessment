package com.post.assestment.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.post.assestment.domain.Entity;
import com.post.assestment.repository.custom.EntityRepositoryCustom;

@Repository
public interface EntityRepository extends MongoRepository<Entity, String>, EntityRepositoryCustom {
	
	List<Entity> findAllOrderByEntityName();

}
