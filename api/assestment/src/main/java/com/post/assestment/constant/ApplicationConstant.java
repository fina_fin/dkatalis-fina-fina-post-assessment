package com.post.assestment.constant;

public class ApplicationConstant {
	public static final String MSG_SUCCESS = "SUCCESS";
	public static final String MSG_ERROR = "ERROR";

	public static final String RESULT = "result";
}
