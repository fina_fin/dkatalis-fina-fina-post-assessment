package com.post.assestment.repository.custom;

import java.util.List;

import com.post.assestment.domain.Entity;

public interface EntityRepositoryCustom {
	public List<Entity> getCountEntity();

}
