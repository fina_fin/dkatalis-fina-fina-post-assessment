package com.post.assestment.domain;

import org.springframework.cache.annotation.Caching;
import org.springframework.data.annotation.Id;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Caching
public class Entity {
	
	@Id
	private String id;

	private String entityId;

	private String entityName;

	private String price;

	private Integer status;

}
