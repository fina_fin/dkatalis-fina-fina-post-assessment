package com.post.assestment.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.post.assestment.constant.ApplicationConstant;
import com.post.assestment.domain.Entity;
import com.post.assestment.exception.BusinessException;
import com.post.assestment.repository.EntityRepository;

@Service
public class EntityServiceImpl {
	
	@Autowired
	private EntityRepository entityRepository;
	
	public Map<String, Object> saveEntity(Entity entity) throws BusinessException {
		List<Integer> statusList = new ArrayList<>();
		statusList.add(0);
		statusList.add(1);
		statusList.add(2);
		if (!statusList.contains(entity.getStatus())) {
			throw new BusinessException("status is not valid");
		} else {
			entityRepository.save(entity);
		}
		return null;
	}
	
	public Map<String, Object> findAll() throws BusinessException {
		Map<String, Object> resultMap = new HashMap<>();
		List<Entity> list = entityRepository.findAllOrderByEntityName();
		resultMap.put(ApplicationConstant.RESULT, setModelToMap(list));
		return resultMap;
	}
	
	private List<Map<String, Object>> setModelToMap(List<Entity> list) {
		List<Map<String, Object>> resultList = new ArrayList<>();

		for (Entity model : list) {
			resultList.add(setModelToMap(model));
		}

		return resultList;
	}

	private Map<String, Object> setModelToMap(Entity model) {
		Map<String, Object> map = new HashMap<>();
		map.put("entityId", model.getEntityId());
		map.put("entityName", model.getEntityName());
		map.put("price", model.getPrice());
		map.put("status", model.getStatus());
		return map;
	}

}
