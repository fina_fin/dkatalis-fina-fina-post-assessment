package com.post.assestment.rest;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.post.assestment.constant.ApplicationConstant;
import com.post.assestment.domain.Entity;
import com.post.assestment.exception.BusinessException;
import com.post.assestment.service.EntityServiceImpl;
import com.post.assestment.utils.ResponseModel;

@RestController
@RequestMapping("/entities")
public class EntityController {
	
	private static final Logger log = LoggerFactory.getLogger(EntityController.class);
	
	@Autowired
	private EntityServiceImpl entityService;

	@PostMapping("/add")
	public ResponseEntity<ResponseModel<Map<String, Object>>> verifiedUser(@RequestBody Entity entity)
			throws BusinessException {
		log.info("add entity");
		return ResponseEntity
				.ok(new ResponseModel<>(ApplicationConstant.MSG_SUCCESS, null, entityService.saveEntity(entity)));
	}

	@GetMapping("/")
	public ResponseEntity<Map<String, Object>> getList() throws BusinessException {
		log.info("get all entities");
		return ResponseEntity.ok(entityService.findAll());
	}

}
