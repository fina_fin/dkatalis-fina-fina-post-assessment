package com.post.assestment.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.post.assestment.rest.EntityController;
import com.post.assestment.service.EntityServiceImpl;

@SpringBootTest
public class EntityControllerTest {

	private MockMvc mockMvc;

	@InjectMocks
	private EntityController entityController;

	@Mock
	private EntityServiceImpl entityService;

	private static final String URL = "/entities";

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(entityController).build();
	}
	
	@Test
	public void getAllEntities() throws Exception {
		mockMvc.perform(get(URL + "/").contentType(MediaType.APPLICATION_JSON_VALUE)).andExpect(status().isOk());
	}

}
