let EntityListDummy = [
    {
      entityId: "1",
      entityName: "Food and Beverages",
      price: 1000,
      status : 0,
      color: 'Red'
    },
    {
      entityId: "2",
      entityName: "Bills",
      price: 2000,
      status : 1,
      color: 'Orange'
    },
    {
      entityId: "3",
      entityName: "Shopping",
      price: 3000,
      status : 2,
      color: 'Green'
    }
  ];
  
  export default EntityListDummy;
  