import React from "react";

function EntityContainer(props) {
  return (
    <tr>
            <td>{props.entityId}</td>
            <td>{props.entityName}</td>
            <td>{props.price}</td>
            <td style={{color: props.color}} >{props.status}</td>
        </tr>
  );
}

export default EntityContainer;
