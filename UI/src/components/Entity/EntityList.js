import React from 'react';
import EntityListDummy from "../DataDummy/EntityListDummy";

import EntityContainer from './EntityContainer';

class EntityList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dataDummy: EntityListDummy
    };
  }

  render() {
    const dataDummy = this.state.dataDummy;
    const datasComponent = [];
    for (var i = 0; i < dataDummy.length; i++) {
      const temp = (
        <EntityContainer
            entityId={dataDummy[i].entityId}
            entityName={dataDummy[i].entityName}
            price={dataDummy[i].price}
            status={dataDummy[i].status}
            color={dataDummy[i].color}
        />
      );
      datasComponent.push(temp);
    }

    return (
      <div className="container">
          <table>
            <tr>
                <td>id</td>
                <td>name</td>
                <td>price</td>
                <td>status</td>
            </tr>
            {datasComponent}
          </table>
      </div>
    );
  }
}

export default EntityList;
