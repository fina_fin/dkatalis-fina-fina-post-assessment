import React from 'react';
import './App.css';
import {
  BrowserRouter as Router, Route, Switch,
} from 'react-router-dom';
import EntityList from "./components/Entity/EntityList";

function App() {
  return (
    <div>
      <Router>
        <Switch>
          <Route path="/" component={EntityList} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
